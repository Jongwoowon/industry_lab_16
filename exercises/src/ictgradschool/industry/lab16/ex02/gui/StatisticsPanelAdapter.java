package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {
	
	/**********************************************************************
	 * YOUR CODE HERE
	 */

	protected StatisticsPanel statisticsPanel;
	protected Course course;

	public StatisticsPanelAdapter(StatisticsPanel statisticsPanel, Course course) {
		this.statisticsPanel = statisticsPanel;
		this.course = course;
		course.addCourseListener(this);
	}

	@Override
	public void courseHasChanged(Course course) {
		statisticsPanel.repaint();
	}
}
