package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */

    private Course course;

    public CourseAdapter(Course course) {
        this.course = course;
    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "student ID";
            case 1:
                return "Surname";
            case 2:
                return "Forename";
            case 3:
                return "Exam";
            case 4:
                return "Test";
            case 5:
                return "Assignment";
            case 6:
                return "Overall";
            default:
                return null;
        }
    }

    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return course.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        StudentResult studentResult = course.getResultAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return studentResult._studentID;
            case 1:
                return studentResult._studentSurname;
            case 2:
                return studentResult._studentForename;
            case 3:
                return studentResult._studentForename;
            case 4:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case 5:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case 6:
                return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Overall);
            default:
                return null;
        }
    }
}